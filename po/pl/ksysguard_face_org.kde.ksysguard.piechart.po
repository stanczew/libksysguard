# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-09 00:44+0000\n"
"PO-Revision-Date: 2021-12-12 11:31+0100\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 22.03.70\n"

#: contents/ui/Config.qml:34
#, kde-format
msgid "Show Sensors Legend"
msgstr "Pokaż legendę mierników"

#: contents/ui/Config.qml:38
#, kde-format
msgid "Start from Angle:"
msgstr "Zacznij od kąta:"

#: contents/ui/Config.qml:43
#, kde-format
msgctxt "angle degrees"
msgid "%1°"
msgstr "%1°"

#: contents/ui/Config.qml:46 contents/ui/Config.qml:59
#, kde-format
msgctxt "angle degrees"
msgid "°"
msgstr "°"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Total Pie Angle:"
msgstr "Całkowity kąt koła:"

#: contents/ui/Config.qml:56
#, kde-format
msgctxt "angle"
msgid "%1°"
msgstr "%1°"

#: contents/ui/Config.qml:64
#, kde-format
msgid "Rounded Lines"
msgstr "Zaokrąglone linie"

#: contents/ui/Config.qml:69
#, kde-format
msgid "Automatic Data Range"
msgstr "Samoustalany zakres danych"

#: contents/ui/Config.qml:73
#, kde-format
msgid "From:"
msgstr "Od:"

#: contents/ui/Config.qml:80
#, kde-format
msgid "To:"
msgstr "Do:"
