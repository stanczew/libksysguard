# Translation for ksysguard_face_org.kde.ksysguard.piechart.po to Euskara/Basque (eu).
# Copyright (C) 2020-2022, This file is copyright:
# This file is distributed under the same license as the libksysguard package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-12-09 00:44+0000\n"
"PO-Revision-Date: 2022-08-06 21:11+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.3\n"

#: contents/ui/Config.qml:34
#, kde-format
msgid "Show Sensors Legend"
msgstr "Erakutsi sentsoreen legenda"

#: contents/ui/Config.qml:38
#, kde-format
msgid "Start from Angle:"
msgstr "Hasi angelutik:"

#: contents/ui/Config.qml:43
#, kde-format
msgctxt "angle degrees"
msgid "%1°"
msgstr "%1°"

#: contents/ui/Config.qml:46 contents/ui/Config.qml:59
#, kde-format
msgctxt "angle degrees"
msgid "°"
msgstr "°"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Total Pie Angle:"
msgstr "Sektorearen angelu osoa:"

#: contents/ui/Config.qml:56
#, kde-format
msgctxt "angle"
msgid "%1°"
msgstr "%1°"

#: contents/ui/Config.qml:64
#, kde-format
msgid "Rounded Lines"
msgstr "Lerro biribilduak"

#: contents/ui/Config.qml:69
#, kde-format
msgid "Automatic Data Range"
msgstr "Datuen barruti automatikoa"

#: contents/ui/Config.qml:73
#, kde-format
msgid "From:"
msgstr "Nondik:"

#: contents/ui/Config.qml:80
#, kde-format
msgid "To:"
msgstr "Nori:"
