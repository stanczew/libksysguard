msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-09-06 00:20+0000\n"
"PO-Revision-Date: 2023-08-02 12:38\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/libksysguard/ksysguardlsofwidgets.pot\n"
"X-Crowdin-File-ID: 43045\n"

#: lsof.cpp:22
#, kde-format
msgctxt "Short for File Descriptor"
msgid "FD"
msgstr "文件描述符"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: lsof.cpp:22 LsofSearchWidget.ui:28
#, kde-format
msgid "Type"
msgstr "类型"

#: lsof.cpp:22
#, kde-format
msgid "Object"
msgstr "对象"

#: LsofSearchWidget.cpp:25
#, kde-format
msgid "Renice Process"
msgstr "重新调整优先级"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: LsofSearchWidget.ui:23
#, kde-format
msgid "Stream"
msgstr "流"

#. i18n: ectx: property (text), widget (KLsofWidget, klsofwidget)
#: LsofSearchWidget.ui:33
#, kde-format
msgid "Filename"
msgstr "文件名"
